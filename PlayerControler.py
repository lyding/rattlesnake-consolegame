from ConsoleGUI.Events import (
        Observer,
        MOUSE_LEFT_CLICK_EVENT,
        MOUSE_RIGHT_CLICK_EVENT)


class PlayerControler:
    def __init__(self, player):
        self.player = player
        self.leftclick_observer = Observer(
                0, [MOUSE_LEFT_CLICK_EVENT], self.leftclick_callback)
        self.rightclick_observer = Observer(
                0, [MOUSE_RIGHT_CLICK_EVENT], self.rightclick_callback)
        self.leftclick_observer.activate()
        self.rightclick_observer.activate()

    def leftclick_callback(self, event):
        self.player.shoot(event.mouse.cspace_position)

    def rightclick_callback(self, event):
        self.player.set_target(event.mouse.cspace_position)
