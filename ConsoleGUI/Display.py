from ConsoleGUI.XLibInterface import center_mouse


class Display():
    '''
    The display represents a two dimensional array of chars stored in a
    string aka a one dimensional char array
    '''
    def __init__(self, width, height):
        '''
        Constructs a new Display
        :param width: The width of the display
        :param height: The height of the display
        :param outlined: A flag if an outer border should be rendered as well
        '''
        self.width = width
        self.height = height
        self.canvas = []
        self.clear()
        center_mouse()

    def clear(self):
        '''
        Clear the canvas
        '''
        self.canvas = [' '] * self.width * self.height

    def draw(self, drawable):
        '''
        Draw a drawable
        '''
        content = drawable.content
        rectangle = drawable.rectangle
        xoffs = rectangle[0]
        yoffs = rectangle[1]
        index = 0
        for y in range(rectangle[3]):
            for x in range(rectangle[2]):
                try:
                    self.canvas[xoffs + x + (yoffs + y) * self.width]\
                        = content[index]
                except IndexError:
                    pass
                index += 1

    def flip(self):
        # Reset the cursor using escape sequences
        print("\033[0;00H")
        print('+' + '-' * self.width + '+')
        index = 0
        for y in range(0, self.height):
            print('|', end='')
            for x in range(0, self.width):
                print(self.canvas[index], end='')
                index += 1
            print('|')
        print('+' + '-' * self.width + '+')
