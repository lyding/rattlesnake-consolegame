import os
from ConsoleGUI.XLibInterface import init_xlib_interface

from ConsoleGUI.Mouse import Mouse
from ConsoleGUI.Display import Display
from ConsoleGUI.GUIElements import Button, HyperLink


if __name__ == '__main__':
    def button_callback(button_instance):
        os.system('firefox')

    init_xlib_interface()
    display = Display(120, 40)
    mouse = Mouse(display)
    button = Button(1, (5, 5, 5, 5), ['#'] * 25, button_callback)
    link = HyperLink(2, (10, 39), 'www.google.de',
                     lambda x: os.system('firefox www.google.de'))
    while(True):
        mouse.update()

        # Clear the display
        display.clear()
        display.draw(button)
        display.draw(link)
        display.draw(mouse)
        display.flip()
