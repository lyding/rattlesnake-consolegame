# THE list of all observers
OBSERVERS = []

# Event definitions
MOUSE_LEFT_CLICK_EVENT = 100
MOUSE_RIGHT_CLICK_EVENT = 101
MOUSE_MOVE_EVENT = 102


class Observer:
    def __init__(self, _id, event_ids, callback):
        self.id = _id
        self.event_ids = event_ids
        self.callback = callback
        self.active = False
        OBSERVERS.append(self)

    def __del__(self):
        OBSERVERS.remove(self)

    def set_callback(self, callback):
        '''
        Set the callback function.
        The callbackfunction should have the form event_id -> None
        '''
        self.callback = callback

    def listen_to(self, event_id):
        '''
        Add an event to listen to
        '''
        self.event_ids.append(event_id)

    def activate(self):
        self.active = True

    def deactivate(self):
        self.active = False


class Event:
    def __init__(self, event_id, **kwargs):
        self.id = event_id
        # Extend the Event class by all attributes speciied in the kwargs
        self.__dict__.update(**kwargs)

    def fire(self):
        for observer in OBSERVERS:
            if observer.active and self.id in observer.event_ids:
                observer.callback(self)


if __name__ == '__main__':
    o1 = Observer(
        0, [1], lambda event: print("This is observer1: " + str(event.id)))
    o2 = Observer(
        1, [1, 2], lambda event: print("This is observer2: " + str(event.id)))
    # Activate the observers
    o1.activate()
    o2.activate()

    # Fire some random observers
    event = Event(1)
    event.fire()
    event = Event(2)
    event.fire()
