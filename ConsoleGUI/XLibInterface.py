from Xlib.display import Display as XDisplay
from Xlib.ext import xinput
from Xlib import X
from Xlib.ext.xtest import fake_input

DISPLAY = None


def init_xlib_interface():
    '''
    Initialise the xlib
    '''
    global DISPLAY
    DISPLAY = XDisplay()


def get_mouse_buttons():
    buttons = []
    for device_info in DISPLAY.xinput_query_device(xinput.AllDevices).devices:
        if not device_info.enabled:
            continue
        if xinput.ButtonClass not in\
                [device_class.type for device_class in device_info.classes]:
            continue
        buttons.append(device_info)

    left = 0
    right = 0
    for button in buttons:
        for device_class in button.classes:
            if xinput.ButtonClass == device_class.type:
                if device_class.state[0]:
                    left = 1
                if device_class.state[2]:
                    right = 1
    return (left, right)


def get_mouse_position():
    data = DISPLAY.screen().root.query_pointer()._data
    return data['root_x'], data['root_y']


def center_mouse():
    # Get the screen resolution
    screen = DISPLAY.screen().root.get_geometry()
    center_x, center_y = screen.width, screen.height
    center_x //= 2
    center_y //= 2
    # Position the mouse in the middle
    fake_input(DISPLAY, X.MotionNotify, x=center_x, y=center_y)
    DISPLAY.sync()
