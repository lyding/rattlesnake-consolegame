from ConsoleGUI.XLibInterface import (
        get_mouse_position,
        get_mouse_buttons)

from ConsoleGUI.Events import (
        Event,
        MOUSE_MOVE_EVENT,
        MOUSE_LEFT_CLICK_EVENT,
        MOUSE_RIGHT_CLICK_EVENT)
from ConsoleGUI.GUIElements import Drawable


class Mouse(Drawable):
    def __init__(self, display):
        self.x = 0
        self.y = 0
        self.cspace_position = [0, 0]
        self.old_position = get_mouse_position()
        self.position = self.old_position
        self.button_state = (0, 0)
        self.old_button_state = (0, 0)
        self.rectangle = (0, 0, 5, 2)
        self.content = ['/'] + ['_'] * 4 + ['\\'] + [' '] * 4
        self.display = display

    def update(self):
        self.position = get_mouse_position()
        self.x, self.y = self.position
        if self.x - self.old_position[0] != 0 or\
                self.y - self.old_position[1] != 0:
            dx = self.x - self.old_position[0]
            dy = self.y - self.old_position[1]
            e = Event(
                    MOUSE_MOVE_EVENT,
                    mouse=self)
            e.fire()
            # Calculate the difference in the console space position
            self.cspace_position[0] += dx // 2
            self.cspace_position[1] += dy // 2
            if self.cspace_position[0] > self.display.width - 1:
                self.cspace_position[0] = self.display.width - 2
            if self.cspace_position[0] < 0:
                self.cspace_position[0] = 0
            if self.cspace_position[1] > self.display.height - 1:
                self.cspace_position[1] = self.display.height - 1
            if self.cspace_position[1] < 0:
                self.cspace_position[1] = 0
            rect_width = 5
            rect_height = 2
            if self.display.width - self.cspace_position[0] < 5:
                rect_width = self.display.width - self.cspace_position[0]
            if self.display.height - self.cspace_position[1] < 2:
                rect_height = self.display.height - self.cspace_position[1]
            self.rectangle = (*self.cspace_position, rect_width, rect_height)
            self.old_position = self.position

        # Get the mouse buttons states and throw events accordingly
        self.button_state = get_mouse_buttons()
        if self.button_state[0] != self.old_button_state[0]:
            if self.button_state[0] == 1:
                e = Event(MOUSE_LEFT_CLICK_EVENT, mouse=self)
                e.fire()
        if self.button_state[1] != self.old_button_state[1]:
            if self.button_state[1] == 1:
                e = Event(MOUSE_RIGHT_CLICK_EVENT, mouse=self)
                e.fire()
        self.old_button_state = self.button_state
