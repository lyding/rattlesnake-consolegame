from ConsoleGUI.Events import (
        Observer,
        MOUSE_MOVE_EVENT,
        MOUSE_LEFT_CLICK_EVENT)


def intersect(position, rectangle):
    if position[0] >= rectangle[0] and\
            position[0] < rectangle[0] + rectangle[2] and\
            position[1] >= rectangle[1] and\
            position[1] < rectangle[1] + rectangle[3]:
        return True
    return False


class Drawable():
    def __init__(self, rectangle, content):
        self.rectangle = rectangle
        self.content = content
        if len(self.content) < self.rectangle[2] * self.rectangle[3]:
            raise Exception('Unsufficient Content')

    def set_content(self, content):
        self.content = content
        if len(self.content) < self.rectangle[2] * self.rectangle[3]:
            raise Exception('Unsufficient Content')


class Button(Drawable):
    def __init__(self, _id, rectangle, content, callback):
        super().__init__(rectangle, content)
        self.observer = Observer(_id, [MOUSE_LEFT_CLICK_EVENT],
                                 self.button_callback)
        self.callback = callback
        # All GUI Elements are active by default
        self.observer.activate()

    def button_callback(self, event):
        '''
        Invoke the stored callback only if the mouse click intersects
        with the button
        '''
        if intersect(event.mouse.cspace_position, self.rectangle):
            self.callback(self)


class HoverElement(Drawable):
    def __init__(self, _id, rectangle, content, callback):
        super().__init__(rectangle, content)
        self.observer = Observer(_id, [MOUSE_MOVE_EVENT], self.button_callback)
        self.callback = callback
        # All GUI Elements are active by default
        self.observer.activate()

    def button_callback(self, event):
        '''
        Invoke the stored callback only if the mouse click intersects
        with the button
        '''
        if intersect(event.mouse.cspace_position, self.rectangle):
            self.callback(self)


class HyperLink(Drawable):
    def __init__(self, _id, position, label, callback):
        super().__init__((*position, len(label) + 2, 1), '~' + label + '~')
        self.observer = Observer(_id, [MOUSE_LEFT_CLICK_EVENT],
                                 self.link_callback)
        self.callback = callback
        # All GUI Elements are active by default
        self.observer.activate()

    def link_callback(self, event):
        '''
        Invoke the stored callback only if the mouse click intersects
        with the link
        '''
        if intersect(event.mouse.cspace_position, self.rectangle):
            self.callback(self)
