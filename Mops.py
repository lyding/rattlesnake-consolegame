from ConsoleGUI.GUIElements import Drawable

PLAYER_SHOT_EVENT = 1000


def sgnm(x):
    return (x > 0) - (x < 0)


class BaseMop:
    def __init__(self, live, position, width, height, content, lag):
        self.live = live
        self.position = list(position)
        self.width = width
        self.height = height
        self.content = content
        self.shape = Drawable((*position, self.width, self.height), content)
        self.lag = lag
        self.lag_timer = 0
        self.alive = True

    def loose_live(self, amount=1):
        self.live -= amount
        if self.live <= 0:
            self.alive = False

    def set_position(self, position):
        self.position = position
        self._sync()

    def move_left(self):
        self.position[0] -= 1
        self._sync()

    def move_right(self):
        self.position[0] += 1
        self._sync()

    def move_up(self):
        self.position[1] -= 1
        self._sync()

    def move_down(self):
        self.position[1] += 1
        self._sync()

    def change_content(self, content):
        self.content = content
        self.shape.set_content(content)

    def _sync(self):
        self.shape.rectangle = (*self.position, self.width, self.height)

    def update(self, ellapsed_time):
        pass


class DefaultMop(BaseMop):
    def __init__(self, position, target):
        self.target = target
        super().__init__(1, position, 1, 1, 'O', 1000)

    def update(self, ellapsed_time):
        self.lag_timer += ellapsed_time

        if not self.alive:
            return

        if self.lag_timer < self.lag:
            return
        if self.target[0] < self.position[0]:
            self.move_left()
        if self.target[0] > self.position[0]:
            self.move_right()
        if self.target[1] < self.position[1]:
            self.move_up()
        if self.target[1] > self.position[1]:
            self.move_down()
        self.lag_timer = 0


class Tanky1Mop(BaseMop):
    def __init__(self, position, target):
        self.target = target
        super().__init__(2, position, 3, 2, ['\\', '_', '/', '/', ' ', '\\'], 1000)

    def update(self, ellapsed_time):
        self.lag_timer += ellapsed_time

        if not self.alive:
            return

        if self.lag_timer < self.lag:
            return
        if self.target[0] < self.position[0]:
            self.move_left()
        if self.target[0] > self.position[0]:
            self.move_right()
        if self.target[1] < self.position[1]:
            self.move_up()
        if self.target[1] > self.position[1]:
            self.move_down()
        self.lag_timer = 0


class Player(BaseMop):
    def __init__(self, max_lives, lag, position, weapon, char):
        self.target = list(position)
        self.weapon = weapon
        super().__init__(max_lives, position, 1, 1, '8', lag)

    def set_target(self, target):
        self.target = list(target)

    def shoot(self, target):
        if abs(target[0] - self.position[0]) > abs(target[1] -
           self.position[1]):
            direction = (sgnm(target[0] - self.position[0]), 0)
        else:
            direction = (0, sgnm(target[1] - self.position[1]))
        self.weapon.shoot(self.position, direction)

    def update(self, ellapsed_time):
        self.lag_timer += ellapsed_time
        self.weapon.update(ellapsed_time)

        if not self.alive:
            return

        if self.lag_timer < self.lag:
            return
        if self.target[0] < self.position[0]:
            self.move_left()
        if self.target[0] > self.position[0]:
            self.move_right()
        if self.target[1] < self.position[1]:
            self.move_up()
        if self.target[1] > self.position[1]:
            self.move_down()
        self.lag_timer = 0


class Bullet(BaseMop):
    def __init__(self, live, lag, position, direction, damage, char):
        self.direction = direction
        self.damage = damage
        super().__init__(live, position, 1, 1, '-', lag)

    def update(self, ellapsed_time):
        self.lag_timer += ellapsed_time
        if self.lag_timer < self.lag:
            return
        self.position[0] += self.direction[0]
        self.position[1] += self.direction[1]
        self.lag_timer = 0
        self._sync()
