from ConsoleGUI.Display import Display
from ConsoleGUI.Mouse import Mouse
from ConsoleGUI.XLibInterface import init_xlib_interface

from Mops import DefaultMop, Tanky1Mop, Player
from PlayerControler import PlayerControler
from Weapon import DefaultWeapon
from Overseer import (
    handle_player_mop_collision,
    handle_bullet_mop_collision,
    handle_bullet_boundary_collision)

from time import time
from random import randint

# Initialise the xlib interface
init_xlib_interface()

running = True
display = Display(60, 20)
mouse = Mouse(display)

# All weapons directly add buttons to the button array
bullets = []
default_weapon = DefaultWeapon(bullets)

player = Player(3, 150, (30, 10), default_weapon, "X")


def pos():
    return randint(0, 60), randint(0, 20)


stages = [
        [DefaultMop(pos(), player.position) for x in range(10)],
        [DefaultMop(pos(), player.position) for x in range(10)] +
        [Tanky1Mop(pos(), player.position)]
]
stage = 0
mops = stages[stage]

player_controler = PlayerControler(player)

old_time = time()
current_time = old_time

while running:
    current_time = time()
    ellapsed_time = (current_time - old_time) * 1000

    mouse.update()
    player.update(ellapsed_time)
    for mop in mops:
        mop.update(ellapsed_time)

    for bullet in bullets:
        bullet.update(ellapsed_time)

    handle_bullet_mop_collision(bullets, mops)
    handle_player_mop_collision(player, mops)
    handle_bullet_boundary_collision(bullets, display)
    if len(mops) == 0:
        stage += 1
        if stage == len(stages):
            print('You won!!!')
            running = False
        else:
            mops = stages[stage]
            bullets.clear()
    if not player.alive:
        print('You lost!!!')
        running = False

    display.clear()
    for mop in mops:
        display.draw(mop.shape)
    for bullet in bullets:
        display.draw(bullet.shape)
    display.draw(player.shape)
    display.draw(mouse)
    display.flip()
    print(f'Stage: {stage}, Live: {player.live}')
    old_time = current_time
