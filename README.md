Rattlesnake
===========

Input
-----
The one and only input device is the mouse.  
The normal mouse cursor is wrapped to a 'virtual'  
mouse cursor within the console space  
Rightclick -> Move  
Leftclick -> Shoot  

Features
--------
1. Different Enemy Mops
2. Different Weapons
3. Multiple Stages

Screenshots
-----------
![Stage2](/source/images/stage2.png "Stage2")
