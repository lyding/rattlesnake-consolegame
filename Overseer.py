def rr_intersect(rect1, rect2):
    if rect1[0] > rect2[0] + rect2[2] or rect2[0] > rect1[0] + rect1[2]:
        return False

    if rect1[1] > rect2[1] + rect2[3] or rect2[1] > rect1[1] + rect1[3]:
        return False
    return True


def handle_player_mop_collision(player, mops):
    for mop in mops:
        if rr_intersect(player.shape.rectangle, mop.shape.rectangle):
            # On Collision the player looses live and is
            # But the mop is removed as well as it did its job
            player.loose_live()
            mops.remove(mop)


def handle_bullet_mop_collision(bullets, mops):
    for bullet in bullets:
        for mop in mops:
            if rr_intersect(bullet.shape.rectangle, mop.shape.rectangle):
                mop.loose_live(bullet.damage)
                bullet.loose_live()
                try:
                    if not mop.alive:
                        mops.remove(mop)
                    if not bullet.alive:
                        bullets.remove(bullet)
                except ValueError:
                    pass


def handle_bullet_boundary_collision(bullets, display):
    for bullet in bullets:
        if bullet.position[0] >= display.width or bullet.position[0] < 0:
            bullets.remove(bullet)
        if bullet.position[1] >= display.height or bullet.position[1] < 0:
            bullets.remove(bullet)
