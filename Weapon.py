from math import inf
from Mops import Bullet


class BaseWeapon:
    def __init__(self, ammo, damage_per_shot, reload_time, char, bullets):
        self.ammo = ammo
        self.damage_per_shot = damage_per_shot
        self.reload_time = reload_time
        self.char = char
        self.bullets = bullets
        self.reload_timer = 0

    def shoot(self, bullets):
        pass

    def update(self, ellapsed_time):
        self.reload_timer += ellapsed_time


class DefaultWeapon(BaseWeapon):
    def __init__(self, bullets):
        super().__init__(inf, 1, 250, '~', bullets)

    def shoot(self, position, direction):
        if self.reload_timer < self.reload_time:
            return
        if self.ammo != inf and self.ammo <= 0:
            return

        self.bullets.append(Bullet(1, 100, position, direction,
                            self.damage_per_shot, self.char))
        self.reload_timer = 0
